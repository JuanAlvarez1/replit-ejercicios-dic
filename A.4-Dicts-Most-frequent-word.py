#__author__ = "Juan     Alvarez"
#__email_ = "juan.v.alvarez@unl.edu.ec"

num = int(input())
palabras ={}
Lista = []
for i in range(num):
    a = list(input().split())
    for t in range(len(a)):
        if a[t] not in palabras:
            palabras[a[t]] = 0
        palabras[a[t]] += 1
for k,v in palabras.items():
    if v >= max(palabras.values()):
        Lista.append(k)
print(sorted(Lista)[0])
