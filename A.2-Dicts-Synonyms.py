#__author__ = "Juan     Alvarez"
#__email_ = "juan.v.alvarez@unl.edu.ec"


n = int(input())
stringA = [[str(s) for s in input().split()] for i in range(n)]
wordpairs = {k: v for (k, v) in stringA}
word = input()
key = 0
value = 0
for key, value in wordpairs.items():
    if wordpairs[key] == word:
        print(key)
    elif key == word:
        print(wordpairs[key])
